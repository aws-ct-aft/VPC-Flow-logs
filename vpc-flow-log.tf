data "aws_vpcs" "current" {}

resource "aws_flow_log" "vpc_flow_log" {
  for_each = {
    for key, id in data.aws_vpcs.current.ids: key => id 
  }          
  iam_role_arn    = "${aws_iam_role.iam_role_vpc_flow_log.arn}"
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = each.value
}

resource "aws_iam_role" "iam_role_vpc_flow_log" {
  name = "iam_role_vpc_flow_log"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_role_policy_vpc_flow_log" {
  name = "iam_role_policy_vpc_flow_log"
  role = "${aws_iam_role.iam_role_vpc_flow_log.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}