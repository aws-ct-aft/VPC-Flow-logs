{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "allow_all_accounts_from_organization_to_put_events",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "events:PutEvents",
      "Resource": "arn:aws:events:us-east-1:077513845273:event-bus/aft-events-from-accounts",
      "Condition": {
        "StringEquals": {
          "aws:PrincipalOrgID": "o-zlzgbl3zxl"
        }
      }
    }]
}