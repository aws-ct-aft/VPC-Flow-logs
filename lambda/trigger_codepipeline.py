import json,boto3

def lambda_handler(event, context):
    # TODO implement
    account_id  = event['detail']['responseElements']['vpc']['ownerId']
    vpc_id      = event['detail']['responseElements']['vpc']['vpcId']  
    print("Account id is "+account_id+" , the vpc id is "+vpc_id)
    
    codepipeline_client = boto3.client('codepipeline')
    response = codepipeline_client.list_pipelines()['pipelines']
    
    for pipeline_name in response:
        if pipeline_name['name'].find(account_id)!=-1:
            codepipeline_response = codepipeline_client.start_pipeline_execution(
                name=pipeline_name['name']
                )
            print(codepipeline_response)
        